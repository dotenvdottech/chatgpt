import "./global.css"
import 'react-quill/dist/quill.snow.css';

import AutoForm from "./AutoForm";

function App() {
  return (
    <AutoForm />
  );
}

export default App;
