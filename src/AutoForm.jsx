import { Button, Card, Form, Input } from "antd";
import { useState } from "react";
import { generateContent } from "./helpers/generate";
import ReactQuill from "react-quill";

const layout = {
  labelCol: { span: 24 },
  wrapperCol: { span: 24 },
};

function AutoForm() {
  const [form] = Form.useForm();

  const [loading, setLoading] = useState(false);
  const [content, setContent] = useState("");

  const onFinish = async (values) => {
    if(!values.keywords) return;
    
    setLoading(true)
    
    const response = await generateContent(values.keywords);
    if(response) {
        form.setFieldValue("content", response)
    }

    setLoading(false)
  };

  return (
    <Card
      title="Fabien-ization"
      style={{ width: 800, margin: "100px auto 0 auto" }}
    >
      <Form
        {...layout}
        form={form}
        onFinish={onFinish}
      >
        <Form.Item
          name="keywords"
          label="Keywords"
        >
          <Input placeholder="Keyword 1, keyword 2, ..." />
        </Form.Item>

        <Button type="primary" htmlType="submit" loading={loading}>Generate</Button>

        <br /><br />
        
        <Form.Item
          name="content"
          label="Content"
        >
          <ReactQuill
            style={{ borderRadius: 10 }}
            value={content}
            onChange={setContent}
            placeholder="Enter the content"
          />
        </Form.Item>
      </Form>
    </Card>
  );
}

export default AutoForm;
