import axios from 'axios';

const OPEN_AI = {
  key: "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",
  api: "https://api.openai.com/v1/engines/text-davinci-002/completions",
};

export const generateContent = async (keywords) => {
  const headers = {
    Authorization: `Bearer ${OPEN_AI.key}`,
  };

  const data = {
    prompt: `Wite a raw html <article>content</article> longer than 1000 words, for and NFT with following keywords: ${keywords}`,
    max_tokens: 4000,
    n: 1,
    stop: "",
    temperature: 1,
  };

  const response = await axios.post(OPEN_AI.api, data, { headers });
  console.log(response.data.choices)
  return response.data.choices[0].text;
};
